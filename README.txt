INSTALLATION
============

MacOS:
 
1. If you don't have it, install Homebrew:
   ruby -e "$(curl -fsSL 
https://raw.githubusercontent.com/Homebrew/install/master/install)"
2. Install python3:
   brew install python3
3. Install JIRA package
   pip3 install jira
4. Set execute permissions on script
   chmod +x timelogger.py
5. Make it so only your user can read your password file
   chmod 0700 credentials.dat

USAGE
=====

1. Modify today.xslx to reflect your daily timelog. There are some 
error fields which will let you know if you make mistakes. Timelogs 
will not be uploaded unless the times add up and all the fields are 
filled out.  The only optional field is the comment, but you will 
still receive a warning if there is not a comment for every entry.

Things you are allowed to edit:
       1. The very first start time entry.
       2. The end-time entries
       3. The issue label
       4. The comment field

Editing anything else may prevent your time log from being parsed.

2. Run the script:
    ./timelogger.py


LICENCE
=======
Don't use this program to commit crimes.  Or do. I'm a readme, not a 
cop.
