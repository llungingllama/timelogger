#!/usr/bin/env python3

import sys
import getpass
import os, os.path, openpyxl, sys
import logging
from termcolor import colored
import datetime 
#from simplecrypt import encrypt, decrypt
#ciphertext = encrypt('password', plaintext)
#plaintext = decrypt('password', ciphertext)

from sheetmanager import TimeLog, Entry

loadFile = './today.xlsx'
blankFile = './.blank.xlsx'

timesheetDate = datetime.date.today()

archiveFile = "./archive/timesheet_{0}.xlsx".format(timesheetDate.strftime("%d%b%Y(%A)"))


loginData = {'Aviata':{'loggedIn':False, 'user':None, 'password':None, 'jira':None},'TrustArc':{'loggedIn':False, 'user':None, 'password':None, 'jira':None}}
class LoginData:
    def __init__(self,company):
        self.company = company
        self.loggedIn = False
        self.user = None
        self.password = None
        self.jira = None
logins = [LoginData('Aviata'),LoginData('TrustArc')]
        
try: 
    from openpyxl.cell import get_column_letter
except ImportError:
    from openpyxl.utils import get_column_letter

   
class Menu:
    """Display an interactive menu."""
    def __init__(self):
        self.choices = {
            "0": self.loginToAviata,
            "1": self.importTimeLog,
            "2": self.reviewTimeLog,
            "3": self.reviewTimeLogSummary,
            "4": self.uploadTimeLog, #simulate
            "5": self.uploadTimeLog, #for realsies
            "6": self.saveCopy,
            "7": self.makeBlank,
            "8": self.quit,
            }
        self.jiras ={}

    def displayMenu(self):
        print("""
            -------------------------
            -----     Menu     ------
            -------------------------
            0. Login to Aviata
            1. Import today's timelog
            2. Print timelog entries
            3. Print timelog summary 
            4. Simulate upload of data to JIRA
            5. Upload data to JIRA
            6. Archive copy of timesheet
            7. Overwrite with blank timesheet
            8. Exit
            """)

    def run(self):
        """Execute Program"""

        #Welcome message
        print(colored("""\nWelcome to the TARDIS: Timelog and Related Data in Sheet. How would you like to manipulate your time today?""", 'red', attrs=['bold']))
        
        while True:
            self.displayMenu()
            self.choice = input("Enter choice:")
            action = self.choices.get(self.choice)
            if action:
                action()
            else:
                print("{0} is not a valid choice".format(self.choice))


    def loginToAviata(self):
        import getpass
        from submissionmanager import checkConnection,login
        company = "Aviata"
        if (checkConnection(company) == True):
            print("Aviata server is up")
            user =  input("Login for {0}: ".format(company))
            password =  getpass.getpass("Password: ")

            try:
                jira = login(user,password,company)
                print("Logged in to {0} successfully.".format(company))
                self.jiras['Aviata'] = jira
            except:
                print("Unable to login as {0} to {1}. Please verify your credentials.".format(user,company))
        else:
            print("Aviata server is down")
        

    def importTimeLog(self):
        from sheetmanager import TimeLog
        
        self.activeTimeLog = TimeLog()
        self.activeTimeLog.fetchWorkLog()
        print("\nTime log imported.\n")

    def reviewTimeLog(self):
        """Print nicely formatted summary of timesheet entries."""
        from sheetmanager import formatTimeDelta
        
        print("\nReviewing Time Log\n")

        try:
            self.activeTimeLog
        except AttributeError:
            print("\nERROR: Leading scientists report you must import a timesheet before you can view its contents.\n")
            return
        
        for entry in self.activeTimeLog.entries:
            print(entry.summaryString())

        print( "\nTotal time is {0}\n".format( formatTimeDelta(self.activeTimeLog.totalTime())) )

    def reviewTimeLogSummary(self):
        """Summarizes time spend on each issue."""
        try:
            self.activeTimeLog
        except AttributeError:
            print("\nERROR: Leading scientists report you must import a timesheet before you can view its contents.\n")
            return
        
        self.activeTimeLog.simplify()


    def uploadTimeLog(self):
        """Add timesheet entries to appropriate JIRAs."""

        if self.choice == "4":
            simulate = True
        if self.choice == "5":
            simulate = False
        
        print("Uploading Aviata Entries:")
        try:
            self.activeTimeLog
        except AttributeError:
            print("\nERROR: Leading scientists report you must import a timesheet before you can upload its contents.\n")
            return

        try:
             self.jiras['Aviata']
        except KeyError:
            print("\nERROR: You have not logged in to a JIRA.\n")
            return
        
        for entry in self.activeTimeLog.entries:
            if entry.uploaded == True:
                print("Skipping the following entry because it has been marked as already uploaded:\n", entry.summaryString())
            elif entry.company == "TrustArc":
                pass
            elif entry.company == "nolog":
                pass
            else:
                if simulate == False:
                    entry.submit(simulate=False, jiras=self.jiras)
                    entry.uploaded = True
                if simulate == True:
                    entry.submit(simulate=True)
            
    
    def saveCopy(self):
        """Make a copy of the file and save to the archive location."""
        import shutil
        shutil.copy2(loadFile, archiveFile)  
        print("\nSaved copy of '{0}' as '{1}'\n".format(loadFile,archiveFile))

    def makeBlank(self):
        """Make a copy of the file and save to the archive location."""
        import shutil
        shutil.copy2(blankFile, loadFile)  
        print("\nCreated blank copy of '{0}'\n".format(loadFile))

        
    def quit(self):
        print("You just leave us behind. Is that what you're going to do to me?")
        sys.exit(0)


if __name__ == "__main__":
    Menu().run()
