import getpass, base64
from jira import JIRA

def saveLogins():
    #import base64
    #base64.b64encode("password")
    input("Login: ")
    getpass.getpass("Password: ")


def loadLogins():
    #import base64
    #base64.b64decode(password)
    pass

def login(user, password, company):
    """Logs in and returns JIRA object."""
    
    if company == "Aviata":
        options  = {'server': 'https://aviatainc.atlassian.net'}
    elif company == "TrustArc":
        options = {'server': 'https://jira.truste.com'}
    else:
        print("Unknown server, returning None")
        return None
    
    jira = JIRA(auth=(user, password), options=options)

    return jira



def checkConnection(company):
    import socket
    
    if company == "Aviata":
        server = "aviatainc.atlassian.net"
    elif company == "TrustArc":
        server = "jira.truste.com"
    else:
        print("Unknown server")
        return False

    try:
        print("Connecting to", server)        
        host = socket.gethostbyname(server)
        s = socket.create_connection((host, 80), 2)
        return True 
    except:
        print("Unable to connect to", server)
        return False
