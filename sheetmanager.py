import datetime, openpyxl
import logging

#store the next available id
lastTimelogID = 0
lastEntryID = 0
jiras = {}

def getInterval(endTime, startTime):
    """Returns timedelta object representing difference of two datetime objects. """
    return ( datetime.datetime.combine(datetime.date.min, endTime) - datetime.datetime.combine(datetime.date.min, startTime) )

def formatTimeDelta(timedelta):
    """Returns string describing hours and minutes"""
    hours = timedelta.seconds // 3600
    minutes = timedelta.seconds // 60 % 60
    if minutes % 10 == minutes:
        zero = "0"
    else:
        zero = ""
    return("{0}h {2}{1}m".format(hours,minutes,zero))


class TimeLog:
    """Represents a daily tabulation of time entries."""

    def __init__(self, entries=[], tags='', timesheetDate=datetime.date.today()):
        self.entries = entries
        self.tags = tags
        self.timesheetDate = datetime.date.today()
        global lastTimelogID
        lastTimelogID += 1
        self.id = lastTimelogID
        self.validate()

    def totalTime(self):
        if len(self.entries) > 0:
            return( getInterval( self.entries[-1].endTime, self.entries[0].startTime ) )
        else:
            print("Timelog must contain at least one entry.")

    def containsTag(self, filter):
        """Searches for timelogs with matching tags."""
        return (filter in self.tags)

    def fetchWorkLog(self):
        """Imports timesheet data from ./today.xslx"""

        firstDataRow = 6 #row where time entries start on spreadsheet
    
        wb = openpyxl.load_workbook('today.xlsx', data_only=True)
        sheet = wb.active

        self.entries = []
    
        for row in range(firstDataRow, sheet.max_row +1):

            startTime = sheet['A' + str(row)].value
            endTime = sheet['C' + str(row)].value
            duration = sheet['E' + str(row)].value
            issueName = sheet['D' + str(row)].value
            comment = sheet['F' + str(row)].value

            #Ignore trailing entries
            if endTime == None and issueName == None and comment == None:
                logging.debug("Skipping incomplete entry", startTime)           
            else:
                if comment == None:
                    comment = ""
                entry = Entry(issueName,startTime,endTime,comment)
                self.entries.append(entry)

        for entry in self.entries:
            print(entry.summaryString())


    def simplify(self):
        """Aggregates time by unique issue label."""
        time = {}
        comments = {}

        for entry in self.entries:
            time[entry.issueName] = time.setdefault(entry.issueName, datetime.timedelta(seconds=0)) + entry.duration
            comments[entry.issueName] = comments.setdefault(entry.issueName, "") +"; " +  str(entry.comment)
        

        for key in time:
            print("  " + (str(key)+":").ljust(5), str(formatTimeDelta(time[key])).ljust(5),"\n"+  comments[key].lstrip("; ").rstrip("; ")+"\n")

            
            
    def validate(self):
        """Check
                  * sum(entry durations) = endTime - startTime
                  * all entries have issues"""
        #result = sum(timedeltas, datetime.timedelta())
        pass

    
    
class Entry:
    """Represents work on an individual issue in the time log."""
    def __init__(self, issueName, startTime, endTime, comment=""):
        self.issueName = issueName

        #Should fetch these from spreadsheet to make it easier for user to customize.  Convention will be names are in ALLCAPS

        global prefixLookup
        prefixLookup = {'AA':'Aviata', 'EPLA':'Aviata', 'AVID':'Aviata', 'AT':'Aviata', 'BROT':'Aviata',  'DELL':'Aviata', 'EPNA':'Aviata', 'FUJI':'Aviata', 'MD':'Aviata', 'SYS':'Aviata', 'XERO':'Aviata','TIMF':'TrustArc', 'DMP':'TrustArc', 'DIM':'TrustArc', 'ELPIS':'TrustArc', 'THEMIS':'TrustArc', 'LUNCH':'nolog', 'BREAK':'nolog'}
        
        self.company = prefixLookup [ self.issuePrefix(issueName.upper()) ]
        
        self.startTime = startTime
        self.endTime = endTime
        self.duration = getInterval(endTime, startTime)
        self.comment = comment       
        self.uploaded = False

        self.validate()

        global lastEntryID
        lastEntryID += 1
        self.id = lastEntryID


    def issuePrefix(self, issueName):
        """Gives the prefix of the label, e.g. TIMF-1919 would return 'TIMF', 'lunch' would return 'lunch' """
        return( issueName.split("-")[0] )
        
        
    def summaryString(self):
        """Returns summary of data included in entry. (as string)"""
        string = "{0}-{1} ({2}): {3} {4}".format(self.startTime.strftime("%I:%M%p"), self.endTime.strftime("%I:%M%p"), formatTimeDelta(self.duration).rjust(6), self.issueName.ljust(10), self.comment)
        return(string)

    

    
    def submit(self, jiras={}, simulate = True, timesheetDate=datetime.date.today()):
        """Loads entry to relevant JIRA. In simulation mode it simply prints steps to the terminal."""
        
        if simulate == False:
            jira = jiras[self.company]
            issue = jira.issue( self.issueName )

            #jira startTime needs full datetime object
            startTime = datetime.datetime.combine(timesheetDate,self.startTime)
            
            jira.add_worklog(issue, timeSpentSeconds=self.duration.seconds, comment=self.comment, started=startTime)
            
        elif simulate == True:
            print("Simulating execution...")
            print("- issue name is {0}".format(self.issueName))
            print("- logging this entry to {0}'s jira".format(self.company))
            print("jira.add_worklog({0.issueName}, timeSpentSeconds={0.duration.seconds}, comment={0.comment}, started={0.startTime}".format(self))

            
    def validate(self):
        """Check for valid configuration upon initialization."""
        assert( self.duration.seconds > 0 ), "Error: negative time interval defined. For {0} at {1}.".format(issueName, startTimeString)



#date = datetime(2018, 07, 30, 18, 35)

#jira = JIRA('https://aviatainc.atlassian.net/',auth=('rshea', 'password'))

#issue = jira.issue('EPLA-1721')
#print(issue.fields.project.key) # 'EPLA'
#print(issue.fields.issuetype.name) #'QA Sub-task'
#print(issue.fields.reporter.displayName) #'Chris Wu'

#jira.add_worklog(issue, timeSpent=None, timeSpentSeconds=None, adjustEstimate=None, newEstimate=None, reduceBy=None, comment=None, started=None, user=None)
#Time format: 2018-07-26T18:05:00.000-0600
#>>> issue.fields.worklog.worklogs[19].started
#'2018-07-26T18:05:00.000-0600'

#add_worklog(issue, timeSpent="", comment="", started=)
